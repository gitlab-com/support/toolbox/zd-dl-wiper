require 'optparse'

module OptionsParser
  def default_options
    # These should match what's set in zd-dl-router & ZenDesk, see
    # https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/#features
    { archive_ext: 'tgz',
      zd_dl_dir: "#{Dir.home}/Downloads/",
      zd_dl_glob: 'zd-*',
      zd_keep_days: 20,
      test_mode: false,
      verbose: false }
  end

  # These keep zd-dl-wiper.rb free of clutter
  def archive_ext  = options[:archive_ext]
  def zd_dl_dir    = options[:zd_dl_dir]
  def zd_dl_glob   = options[:zd_dl_glob]
  def zd_keep_days = options[:zd_keep_days]
  def test_mode    = options[:test_mode]
  def verbose      = options[:verbose]

  def parse_options!
    self.options = default_options

    OptionParser.new do |opts|
      opts.banner = 'Usage: ruby ./zd-dl-wiper.rb [options]'
      opts.separator ""
      opts.separator "A companion for GitLab.com/gitlab-com/support/toolbox/zd-dl-router that removes unused ZenDesk downloads."
      opts.separator ""
      opts.separator "Specific options:"

      opts.on('-e <EXT>', '--archive-extension <EXT>',
              'File extension to use for temporary archiving of zd-dl-route\'d items.') do |ext|
        options[:archive_ext] = ext
      end

      opts.on('-d <DIR>', '--downloads-dir <DIR>',
              'Specify the directory, in which zd-dl-router creates the ticket-specific subfolders.') do |dir|
        options[:zd_dl_dir] = dir
      end

      opts.on('-z <GLOB>', '--zd-glob <GLOB>',
              'Glob pattern by which to find the ticket-specific items.') do |glob|
        options[:zd_dl_glob] = glob
      end

      opts.on('-k <INT>', '--days-to-keep <INT>',
              'Number of days to keep items.') do |days|
        options[:zd_keep_days] = days.to_i
      end

      opts.on('-t', '--test-mode',
              'Only list actions that would be taken in non-test mode.') do
        options[:test_mode] = true
      end

      opts.on('-v', '--verbose',
              'Also log items that require no action. By default, those are not logged.') do
        options[:verbose] = true
      end

      opts.on('-h', '--help', 'Prints this help') do
        puts opts
        exit
      end
    end.parse!(@args)
  end
end
