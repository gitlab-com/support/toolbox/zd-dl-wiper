# frozen_string_literal: true

# Superclass to register & manufacture inode-specific subclasses.
class Wiper
  def wipe!
    raise(NoMethodError,
          "No subclass that can handle the requested inode #{inode} has been registered! Aborting…")
  end

  def delete!
    log('Cleaning up', inode)
    return if test_mode

    begin
      FileUtils.remove_entry_secure(inode)
    rescue Errno::ENOENT => e
      puts e.message
    end
  end

  def log(action, inode) = puts("#{action} #{inode} now...")

  # Factory & registry for inode-specific subclasses

  def self.for(inode, archive_ext, test_mode)
    subclasses
      .find { |sc| sc.handles?(inode) }
      .new(inode, archive_ext, test_mode)
  end

  def self.subclasses    = @subclasses ||= []
  def self.register(sc)  = subclasses.prepend(sc)
  def self.inherited(sc) = register(sc)
  register(self)

  def initialize(inode, archive_ext, test_mode)
    @inode = inode
    @filename = "#{inode}.#{archive_ext}"
    @test_mode = test_mode
  end

  attr_reader :filename, :inode, :test_mode
end

# Subclass for the subdirectories created by zd-dl-router
class DownloadSubdir < Wiper
  def wipe!
    archive!
    delete!
  end

  def archive!
    log('Compressing', "#{inode} to #{filename}")
    return if test_mode

    target = File.open(filename, 'wb')
    target = Zlib::GzipWriter.new(target)
    begin
      Minitar.pack(inode, target)
    rescue Errno::ELOOP => e
      puts e.message
      delete!
    end
    # https://github.com/halostatue/minitar#label-Synopsis
  end

  def self.handles?(inode) = File.ftype(inode) == 'directory'
end

# Subclass for the archive files created by DownloadSubdir.wipe!
class BufferArchive < Wiper
  def wipe! = delete!
  def self.handles?(inode) = File.ftype(inode) == 'file'
end
