#!/bin/env sh

TMP=RM.md
FIN=README.md

# Start temporary file with upper part
rg --no-line-number --max-count 1 \
  --before-context=99 \
  '## List of options' "$FIN" \
  >"$TMP"

{
  echo ""
  ruby ./zd-dl-wiper.rb --help |
    grep --extended-regex '^\s+-'
  echo ""
  rg --no-line-number --max-count 1 \
    --after-context=6 \
    '^---$' "$FIN"
} >>"$TMP"

# Add changes to same commit
mv -f "$TMP" "$FIN"
git add "$FIN"
