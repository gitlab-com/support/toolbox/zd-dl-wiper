#!/bin/env sh

while true; do
  ruby ./zd-dl-wiper.rb \
    --archive-extension 'tar.gz' \
    --downloads-dir ~/Downloads \
    --zd-glob 'zd-*' \
    --days-to-keep 10 \
    --test-mode |
    column

  echo ''
  date
  sleep 2
done
