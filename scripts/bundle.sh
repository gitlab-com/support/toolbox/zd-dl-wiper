#!/bin/env sh

bundle update &&
  bundle install &&
  bundle audit
