# frozen_string_literal: true

require 'minitar' # esp. https://git.io/Jqx7I
require 'time_difference'
require 'zlib'
require_relative './options-parser'
require_relative './zd-dl-wipers'

# A companion for GitLab.com/gitlab-com/support/toolbox/zd-dl-router
# that removes unused ZenDesk downloads. See README.md for more details.
class ZenDeskDownloadsWiper
  include OptionsParser
  attr_accessor :options

  def initialize(args)
    @args = args
    parse_options! # creates @options
  end

  def run!
    too_short unless zd_keep_days >= 1

    Dir.chdir(zd_dl_dir)

    list_inodes
      .select { |inode| unused?(inode) }
      .sort_by(&:length)
      .reverse
      .map { |inode| Wiper.for(inode, archive_ext, test_mode) }
      .each(&:wipe!)
  end

  private

  def unused?(inode)
    log("Checking", inode) if verbose
    unused = zd_keep_days <
      TimeDifference
      .between(Time.now, File.mtime(inode))
      .in_days
    log("Ignoring", inode) if !unused && verbose
    unused
  end

  def list_inodes
    Dir.glob(zd_dl_glob)
  end

  def too_short = raise(ArgumentError, "Too short interval: #{zd_keep_days}! Want to keep downloads for at least 1 day.")

  def log(action, inode) = puts("#{action} #{inode} now...")
end

ZenDeskDownloadsWiper.new(ARGV).run!
